﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ASP.NET_MVC5_Bootstrap_3._3._1_LESS.Models
{
    [DisplayName("文章")]
    public class News
    {
        public int Id { get; set; }

        [Required]
        [DisplayName("标题")]
        public string Title { get; set; }

        [DisplayName("发布日期")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime PubTime { get; set; }

        [Required]
        [DisplayName("内容")]
        public string Content { get; set; }
    }
}