// <auto-generated />
namespace ASP.NET_MVC5_Bootstrap_3._3._1_LESS.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class InitialNews : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(InitialNews));
        
        string IMigrationMetadata.Id
        {
            get { return "201501010937063_InitialNews"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
