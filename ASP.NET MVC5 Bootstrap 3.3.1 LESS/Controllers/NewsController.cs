﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using ASP.NET_MVC5_Bootstrap_3._3._1_LESS.Models;
using System.Net;

namespace ASP.NET_MVC5_Bootstrap_3._3._1_LESS.Controllers
{
    public class NewsController : Controller
    {
        private ProjectContext db = new ProjectContext();
        // GET: News
        public ActionResult Index(int page = 1)
        {
            return View(db.Newss.OrderByDescending(news => news.Id).ToList().ToPagedList(pageNumber: page, pageSize: 10));
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            News news = db.Newss.Find(id);
            if (news == null)
            {
                return HttpNotFound();
            }
            return View(news);
        }
    }
}