#README#

###项目概述###

这是一个基于 ASP.NET MVC 5 + Bootstrap 3.3.1 + UEditor 1.4.3 构建的简易新闻发布系统，包含用户注册、新闻发布模块。 

* 户注册模块为系统自动生成的，提供外接第三方服务登陆功能。目前仅提供Google和Facebook登录。
* 新闻发布模块的使用限定为登录用户。

###开发环境###
    
* **开发软件：** Visual Studio Ultimate 2015 Preview
* **操作系统：** Windows 8.1 Home Premium Edition
* **版本控制：** Bitbucket + SourceTree
* **其他信息：** MVC 5，Entity Framework 6，LocalDB v12.0，Bootstrap 3.3.1，UEditor 1.4.3

###部署说明###

* 如果想要运行项目，则使用的开发环境的版本不能低于上面列出的版本。
* 此项目中有一个名为 **PrivateSetting.config** 的配置文件，里面是<appSettings>的相关内容，没有被包含进来。运行时需要自行创建该文件。具体可以参照下面的内容。其中主要是第三方登陆的一些密钥和ID。


```
#!XML

<appSettings file="PrivateSettings.config">
  <add key="GOOGLE_CLIENT_ID" value="你的Google客户端ID" />
  <add key="GOOGLE_CLIENT_SECRET" value="你的Google客户端密钥" />

  <add key="FACEBOOK_APP_ID" value="你的Facebook应用ID" />
  <add key="FACEBOOK_APP_SECRET" value="你的Facebook应用密钥" />
  
  <add key="webpages:Version" value="3.0.0.0" />
	<add key="webpages:Enabled" value="false" />
	<add key="ClientValidationEnabled" value="true" />
	<add key="UnobtrusiveJavaScriptEnabled" value="true" />
</appSettings>
```


###目前不足###

因为只是一个简单的新闻发布系统，所以一切都是按简单了来处理。不过还是有挺多不足之处。

    1.任何用户只要注册即可发布新闻。

    2.新闻浏览下面应该有“上一篇”、“下一篇”的链接。

###作者信息###

* **作者：** 汤和果
* **E-mail：** [hgtang93@hotmail.com](mailto:hgtang93@hotmail.com)
* **地址：** 计算机科学与工程系，数理与信息工程学院，浙江师范大学，浙江省金华市迎宾大道688号